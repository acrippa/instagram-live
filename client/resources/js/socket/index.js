import Vue from 'vue'
import VueNativeSock from 'vue-native-websocket'

import store from '../store'
import { INSTABOX_CONFIG } from '../config'

Vue.use(VueNativeSock, INSTABOX_CONFIG.WS_URL, {
    reconnection: false,
    format: 'json',
    store: store,
    connectManually: true,
    })