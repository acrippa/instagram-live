import { INSTABOX_CONFIG } from '../config'

export const statistics = {
    namespaced: true,

    state: {
        likes: [0],
        bursts: [0],
        heartbeat: {
            total_unique_viewer_count: 0,
            viewer_count: 0
        },
        total_viewers: 0,
        viewers: [0],
    },

    actions: {
        status: ({ commit }, stats ) => {
            commit('likes', stats.likes)
            commit('bursts', stats.bursts)
            commit('viewers', stats.viewers)
            commit('total_viewers', stats.total_viewers)
        }
    },

    mutations: {
        likes: (state, likes) => state.likes.push(likes),
        bursts: (state, bursts) => state.bursts.push(bursts),
        viewers: (state, viewers) => state.viewers.push(viewers),
        total_viewers: (state, total_viewers) => state.total_viewers = total_viewers
    }
}