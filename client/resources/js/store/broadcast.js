export const broadcast = {
    namespaced: true,

    state: {
        destination: {
            type: 'direct',
        },
        source: {
            ingest: 'ingest',
            audio: {
                options: '-f alsa',
                url: 'pulse',
                bitrate: '128k',
            },
            video: {
                options: '-f v4l2',
                url: '/dev/video0',
                bitrate: '1500k',
            }
        },
        status: {
            streamUploadUrl: null,
            broadcasting: false,
            broadcastId: null,
            info: null,
            streamUrl: null,
            streamKey: null,
        },
    },

    actions: {
        create: ( { state, dispatch } ) => {
            let data = {
                action: 'createBroadcast',
                payload: {
                    source: state.source,
                    destination: state.destination,
                }
            }
            dispatch('sendMessage', data, {root:true})
        },
        start: ( { dispatch } ) => {
            let data = {
                action: 'startBroadcast'
            }
            dispatch('sendMessage', data, {root:true})
        },
        stop: ({ dispatch }, save) => {
            let data = {
                action: 'stopBroadcast',
                payload: {
                    save: save,
                }
            }
            dispatch('sendMessage', data, {root:true})
        }
    },

    mutations: {
        creating: (state, status) => state.status = _.assign(state.status, { 
            streamUploadUrl : status.streamUploadUrl ,
            streamUrl : status.streamUrl, 
            streamKey : status.streamKey,
            broadcastId: status.broadcastId,
            broadcasting: status.broadcasting,
            comments: status.comments,
        }),
        broadcasting: (state, status) => state.status = _.assign(state.status, {
            broadcasting: status.broadcasting,
            info: status.info,
        }),
        stopped: state => state.status.broadcasting = false,
    }
}