import swal from 'sweetalert2'

export const user = {
    namespaced: true,

    state: {
        user: {
            accountId: null,
            username: 'olivier.franz',
            password: 'DioInu93'
        },
        loggedIn: false,
        twofactor: false,
    },

    actions: {
        login: ( { dispatch }, payload ) => {
            let data = {
                action: 'login',
                payload: payload
            }
            dispatch('sendMessage', data, {root:true})
        },
        twofactor: ( { dispatch }, payload ) => {
            swal({
                type: 'question',
                title: 'Enter the two factor code',
                input: 'text',
                showCancelButton: true,
                inputValidator: (value) => {
                    return !value && 'You need to write something!'
                }
            }).then( result => {
                let data = {
                    action: 'twofactor',
                    payload: { code: result.code }
                }
                dispatch('sendMessage', data, {root:true})
            })
        },
        challenge: ( { dispatch }, payload ) => {
            swal({
                type: 'question',
                title: 'Select challenge method',
                input: 'radio',
                inputOptions: {
                    'sms': 'SMS',
                    'email': 'Email'
                },
                showCancelButton: true,
                inputValidator: (value) => {
                    return !value && 'You need to choose something!'
                }
            }).then( result => {
                let data = {
                    action: 'challenge',
                    payload: { method: result.value}
                }
                dispatch('sendMessage', data, {root:true})
            })
        },
        challenged: ( { dispatch } ) => {
            swal({
                type: 'question',
                title: 'Enter the code you received',
                input: 'text',
                showCancelButton: true,
                inputValidator: (value) => {
                    return !value && 'You need to write something!'
                }
            }).then( result => {
                let data = {
                    action: 'challengeCode',
                    payload: { code: result.code }
                }
                dispatch('sendMessage', data, {root:true})
            })
        },
        retry: () => {
            swal({
                type: 'warning',
                title: 'Suspicious Login!',
                text: 'No clue if challenge accepted, you should re-run the script and retry!',
            }).then( response => {
                this.$store.dispatch('resetError')
            })
        },
        close: () => {
            swal({
                type: 'warning',
                title: 'Suspicious Login!',
                text: 'Account challenge successful, you should re-run the script!',
            }).then( response => {
                this.$store.dispatch('resetError')
            })

        }
    },

    mutations: {
        login: ( state, message ) => {
            state.user.accountId = message.accountId
            state.loggedIn = true
            state.twofactor = false
        },
        logout: state => { 
            state.loggedIn = false
            state.twofactor = false 
        },
        status: (state, status) => {
            if(status.loggedIn){
                state.user.username = status.username
                state.user.password = status.password
                state.loggedIn = true
            }
        }
    }
}