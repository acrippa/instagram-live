export const comment = {
    namespaced: true,

    state: {
        comments: [],
        comment: {
            text: ''
        },
        enabled: true,
    },

    actions: {
        send: ( { state, dispatch, commit } ) => {
            if(state.comment.text){
                let data = {
                    action: 'sendComment',
                    payload: {
                        message: state.comment.text,
                    }
                }
                dispatch('sendMessage', data, {root:true})
                commit('reset')
            }
        },

        toggle: ( { state, dispatch } ) => {
            let data = {
                action: 'toggleComments'
            }
            dispatch('sendMessage', data, {root:true})
        },
    },

    mutations: {
        comments: ( state, response ) => state.comments = state.comments.concat(response.comments),
        comment: ( state, response ) => state.comments.push(response.comment),
        toggle: ( state, response ) => state.enabled = response.comments,
        reset: ( state ) => state.comment.text = '',
    }
}