import { INSTABOX_CONFIG } from '../config'

export const wsocket = {
    namespaced: true,

    state: {
        address: INSTABOX_CONFIG.WS_URL,
    },

    actions: {
        connect: ( { state, commit }, vm ) => {
            vm.$connect( state.address)
        },
        disconnect: ( context, vm ) => {
            vm.$disconnect()
        }
    },
}