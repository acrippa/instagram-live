import Vue from 'vue'
import Vuex from 'vuex'
Vue.use( Vuex )

import { broadcast } from './broadcast'
import { comment } from './comment'
import { statistics } from './statistics'
import { user } from './user'
import { wsocket } from './wsocket'

export default new Vuex.Store({
    state: {
        socket: {
            isConnected: false,
            message: '',
            reconnectError: false,
            error: null,
        },
        error: false,
    },

    modules: {
        broadcast,
        comment,
        statistics,
        user,
        wsocket,
    },

    actions: {

        sendMessage: (context, message) => {
            Vue.prototype.$socket.sendObj(message)
        },

        resetError: ({ commit }) => {
            commit( 'error', false )
        }

    },

    mutations:{

        error: ( state, error ) => state.error = error,

        SOCKET_ONOPEN (state, event)  {
            Vue.prototype.$socket = event.currentTarget
            state.socket.isConnected = true
        },

        SOCKET_ONCLOSE (state, event)  {
            state.socket.isConnected = false
        },

        SOCKET_ONERROR (state, event)  {
            state.socket.error = event
        },

        SOCKET_ONMESSAGE (state, message)  {
            if(message.status != 200){
                state.error = true
            }
            state.socket.message = message
        },

        SOCKET_RECONNECT(state, count) {
            //console.info(state, count)
        },

        SOCKET_RECONNECT_ERROR(state) {
            state.socket.reconnectError = true;
        },
    }

})