require('./bootstrap')

import Vue from 'vue'
import socket from './socket'
import store from './store'

Vue.component('app', require('./components/App').default)

const app = new Vue({
    store,
    el: '#app',
});
