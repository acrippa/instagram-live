<?php

namespace App\Http\Middleware;

use Closure;

class AuthToken
{

    public function handle($request, Closure $next)
    {
        if(!$request->hasHeader('Authorization')) {
            return response()->json('Authorization Header not found', 401);
        }

        $token = $request->bearerToken();

        if($request->header('Authorization') == null || $token == null) {
            return response()->json('No token provided', 401);
        }

        $this->retrieveAndValidateToken($token);

        return $next($request);
    }

    public function retrieveAndValidateToken($token)
    {
        if($token != env('API_TOKEN')){
            return response()->json('Wrong token provided', 401);
        }
    }

}