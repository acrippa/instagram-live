<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use App\Models\Broadcast;
use App\Models\Comment;
use App\Models\Statistic;

class BroadcastController extends Controller
{
    public function create(Request $request)
    {
        try{
            Broadcast::create($request->all());
        }catch(\Exception $e){
            return response()->json(["message" => $e->getMessage()], 500);
        }
    }

    public function stop(Request $request, $broadcastId)
    {
        try{
            $broadcast = Broadcast::find($broadcastId);
            $broadcast->ended_at = Carbon::now();
            $broadcast->save();
        }catch(\Exception $e){
            return response()->json(["message" => $e->getMessage()], 500);
        }
    }

    public function statistics(Request $request, $broadcastId)
    {
        try{
            $stat = new Statistic();
            $stat->broadcast_id = $broadcastId;
            $stat->likes = $request->input('likes');
            $stat->bursts = $request->input('bursts');
            $stat->viewers = $request->input('viewers');
            $stat->total_viewers = $request->input('total_viewers');
            $stat->save();
        }catch(\Exception $e){
            return response()->json(["message" => $e->getMessage()], 500);
        }
    }

    public function comments(Request $request, $broadcastId)
    {
        try{
            if($request->has('comments')){
                $comments = $request->input('comments');
                foreach ($comments as $comment) {
                    Comment::create([
                        'id' => $comment['id'],
                        'broadcast_id' => $broadcastId,
                        'username' => $comment['username'],
                        'comment' => $comment['text'],
                        'created_at' => new Carbon($comment['timestamp'])
                    ]);
                }
            }
        }catch(\Exception $e){
            return response()->json(["message" => $e->getMessage()], 500);
        }
    }
}
