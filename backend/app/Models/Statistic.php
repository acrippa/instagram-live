<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Statistic extends Model
{
    protected $guarded = [];

    public function broadcast()
    {
        return $this->belongsTo('App\Models\Broadcast');
    }

}