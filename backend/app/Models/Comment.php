<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Comment extends Model
{
    public $incrementing = false;
    
    protected $keyType = 'string';

    protected $guarded = [];

    public function broadcast()
    {
        return $this->belongsTo('App\Models\Broadcast');
    }

}