<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Broadcast extends Model
{
    public $incrementing = false;
    
    protected $keyType = 'string';

    protected $guarded = [];

    public function comments()
    {
        return $this->hasMany('App\Models\Comment');
    }

    public function statistics()
    {
        return $this->hasMany('App\Models\Statistic');
    }

}