<?php

require __DIR__.'/vendor/autoload.php';

use Ratchet\App;
use React\EventLoop\Factory as LoopFactory;
use BigBadWolf\InstagramServer;

$dotenv = Dotenv\Dotenv::create(__DIR__);
$dotenv->load();

$loop = LoopFactory::create();

$app = new Ratchet\App(getenv('HTTP_HOST'), getenv('WS_PORT'), getenv('WS_ADDRESS'), $loop);
$app->route('/', new InstagramServer($loop), array('*'));
$app->run();