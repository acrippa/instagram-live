<?php

namespace BigBadWolf\Broadcaster;

/**
 * Wowza Streaming Engine™ Broadcaster
 * Broadcaster to use the Wowza engine to push an existing 
 * rtmp streaming on your platform to the Instagram CDN
 * The Wowza ingest stream is in the form instagram_$streamName.sdp
 * It creates a target with the name instagram_$streamName.target
 *
 * Wowza documentation
 * https://www.wowza.com/docs/stream-targets-query-examples-push-publishing
 */
class WowzaBroadcaster implements BroadcasterInterface
{
    private $running = false;
    private $settings;
    private $url;
    private $wowza;

    public function __construct()
    {
        $wowzaHost = getenv('WOWZA_HOST');
        $wowzaUsername = getenv('WOWZA_USERNAME');
        $wowzaPassword = getenv('WOWZA_PASSWORD');
        $settings = new Com\Wowza\Entities\Application\Helpers\Settings();
        $settings->setHost($wowzaHost);
        $settings->setUsername($wowzaUsername);
        $settings->setPassword($wowzaPassword);
        $this->wowza = new Com\Wowza\StreamTarget($settings, 'instagram');
    }

    public function addSource($source)
    {
        $this->source = $source;
    }

    public function addDestination($destination)
    {
        $this->destination = $destination;
    }

    public function start()
    {
        $streamName = str_replace('rtmp://live-upload.instagram.com:80/rtmp/', '', $this->destination);
        $this->wowza->create(
            'instagram_' . $this->source->ingest . '.sdp',
            'instagram_' . $this->source->ingest . '.target',
            'rtmp',
            'live-upload.instagram.com',
            null, 
            null,
            $streamName
        );
        $this->running = true;
    }

    public function isRunning()
    {
        return $this->running;
    }

    public function stop()
    {
        $this->wowza->remove('instagram_' . $this->source->ingest . '.target');
        $this->running = false;
    }
}