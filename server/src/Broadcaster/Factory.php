<?php

namespace BigBadWolf\Broadcaster;

class Factory
{
    public static function create(String $type)
    {
        switch($type){
            case 'ffmpeg':
                return new FfmpegBroadcaster();
            break;
            case 'wowza': 
                return new WowzaBroadcaster();
            default:
                return new DirectBroadcaster();
            break;
        }
    }
}