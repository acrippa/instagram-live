<?php

namespace BigBadWolf\Broadcaster;

/**
 * NGINX Broadcaster
 * It uses an existing nginx server with rtmp module
 * Just a dummy class, it works exactly like the DirectBroadcaster
 * Instead of streaming directly to instagram you use nginx as a proxy
 * 
 * nginx.conf example
 * rtmp {
 *   server {
 *     listen 1935;
 *     chunk_size 4096;
 *     application instagram {
 *       live on;
 *       record off;
 *       # If you want to use authentication
 *       # make HTTP request & use HTTP retcode
 *       # to decide whether to allow publishing
 *       # from this connection or not
 *       # on_publish http://$authServer:$authPort/$authEndpoint;
 *
 *       push rtmp://live-upload.instagram.com:80/rtmp/;
 *     }
 *   }
 * }
 * with this configuration you would stream to rtmp://$server:1935/instagram/$streamKey
 *
 */
class NGINXBroadcaster implements BroadcasterInterface
{
    private $running = false;

    public function addSource($source)
    {
    }

    public function addDestination($destination)
    {
    }

    public function start()
    {
        $this->running = true;
    }

    public function isRunning()
    {
        return $this->running;
    }

    public function stop()
    {
        $this->running = false;
    }
}