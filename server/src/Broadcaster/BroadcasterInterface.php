<?php

namespace BigBadWolf\Broadcaster;

interface BroadcasterInterface
{

    public function addSource($source);

    public function addDestination($destination);

    public function start();

    public function isRunning();

    public function stop();

}