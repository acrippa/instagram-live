<?php

namespace BigBadWolf\Broadcaster;

use \InstagramAPI\Media\Video\FFmpeg;

/**
 * FFMPEG Broadcstaer
 * Broadcaster to use ffmpeg to stream to instagram
 * Copy-Paste from the example
 * https://github.com/mgp25/Instagram-API/blob/master/examples/liveBroadcast.php
 */
class FfmpegBroadcaster implements BroadcasterInterface
{

    private $ffmpegPath;
    private $ffmpeg;
    private $options = '-rtbufsize 256M -re %s -i %s %s -i %s -acodec libmp3lame -ar 44100 -b:a %s -filter:v "scale=1280:-1,crop=406:720" -pix_fmt yuv420p -profile:v baseline -s 406x720 -bufsize 6000k -b:v %s -maxrate %s -deinterlace -vcodec libx264 -preset veryfast -g 30 -r 30 -f flv ';
    private $source;
    private $destination;
    private $process;

    public function __construct()
    {
        $this->ffmpeg = FFmpeg::factory($this->ffmpegPath);
    }

    public function addSource($source)
    {
        $this->source = $source;
    }

    public function addDestination($destination)
    {
        $this->destination = $destination;
    }

    public function start()
    {
        $this->process = $this->ffmpeg->runAsync(sprintf($this->options, 
            $this->source->audio->options,
            \Winbox\Args::escape($this->source->audio->url),
            $this->source->video->options,
            \Winbox\Args::escape($this->source->video->url),
            $this->source->audio->bitrate,
            $this->source->video->bitrate,
            $this->source->video->bitrate,
            \Winbox\Args::escape($this->destination)
        ));
    }

    public function isRunning()
    {
        return $this->process->isRunning();
    }

    public function stop()
    {
        $this->process->stop();
    }
}