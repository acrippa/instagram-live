<?php

namespace BigBadWolf\Broadcaster;

/**
 * Direct broadcaster
 * It just gives you the instagram stream URL to use in your favorite
 * streaming software (OBS, VLC, Livestream, ...)
 */
class DirectBroadcaster implements BroadcasterInterface
{
    private $running = false;

    public function addSource($source)
    {
    }

    public function addDestination($destination)
    {
    }

    public function start()
    {
        $this->running = true;
    }

    public function isRunning()
    {
        return $this->running;
    }

    public function stop()
    {
        $this->running = false;
    }
}