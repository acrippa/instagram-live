<?php

namespace BigBadWolf\Broadcaster;

use Aws\MediaLive\MediaLiveClient;
/**
 * AWS Elemental MediaLive Broadcaster
 * Broadcaster that use the AWS platform
 * to push the input to the Instagram CDN
 * it creates an input, a channel to which the input 
 * is attached and when the live is stopped delete everything
 *
 * AWS documentation
 * https://docs.aws.amazon.com/aws-sdk-php/v3/api/api-medialive-2017-10-14.html
 *
 * It still has work to do
 */
class AWSMediaLiveBroadcaster implements BroadcasterInterface
{
    private $channel;
    private $input;
    private $medialive;
    private $running = false;
    private $settings;
    private $url;

    public function __construct()
    {
        $this->medialive = new MediaLiveClient([
            'key' => getenv('AWS_KEY'),
            'secret' => getenv('AWS_SECRET'),
            'region' => getenv('AWS_REGION'),
        ]);
    }

    public function addSource($source)
    {
        $this->source = $source;
        $this->input = $this->medialive->createInput([
            'Destinations' => [
                [
                    'StreamName' => $source['source1'],
                ],
                [
                    'StreamName' => $source['source2'],
                ],
            ],
            'InputSecurityGroups' => [ getenv('AWS_SECURITY_GROUP') ],
            'Name' => $source['name'],
            'Type' => 'RTMP_PUSH',
        ])->get('Input');
    }

    public function addDestination($destination)
    {
        $this->destination = $destination;
        $split = explode("/", $this->destination);
        $this->channel = $this->medialive->createChannel([
            'Destinations' => [
                [
                    'Id': 'instagram',
                    'Settings' => [
                        [
                            'StreamName' => end($split),
                            'Url' => 'rtmp://live-upload.instagram.com:80/rtmp/',
                        ],
                    ],
                ],
            ],
            'EncoderSettings' => [
                'AudioDescriptions' => [
                    [
                        'AudioSelectorName' => 'audio',
                        'AudioTypeControl' => 'FOLLOW_INPUT',
                        'LanguageCodeControl' => 'FOLLOW_INPUT',
                        'Name' => 'audio',
                    ],
                ],
                'OutputGroups' => [
                    [
                        'Name' => 'Instagram',
                        'OutputGroupSettings' => [
                            'RtmpGroupSettings' => [
                                'AuthenticationScheme' => 'COMMON',
                                'CacheFullBehavior' => 'DISCONNECT_IMMEDIATELY',
                                'CacheLength' => 30,
                                'InputLossAction' => 'EMIT_OUTPUT',
                                'RestartDelay' => 15,
                            ],
                        ],
                        'Outputs' => [
                            [
                                'AudioDescriptionNames' => ['audio'],
                                'OutputSettings' => [
                                    'RtmpOutputSettings' => [
                                        'CertificateMode' => 'VERIFY_AUTHENTICITY',
                                        'ConnectionRetryInterval' => 2,
                                        'Destination' => [
                                            'DestinationRefId' => 'instagram',
                                        ],
                                        'NumRetries' => 10,
                                    ],
                                ],
                                'VideoDescriptionName' => 'video',
                            ],

                        ],
                    ],

                ],
                'TimecodeConfig' => [
                    'Source' => 'EMBEDDED',
                ],
                'VideoDescriptions' => [
                    [
                        'Name' => 'video',
                        'RespondToAfd' => 'NONE',
                        'ScalingBehavior' => 'DEFAULT',
                    ],

                ],
            ],
            'InputAttachments' => [
                [
                    'InputAttachmentName' => 'instagram',
                    'InputId' => $this->input['id'],
                    'InputSettings' => [
                        'DeblockFilter' => 'DISABLED',
                        'DenoiseFilter' => 'DISABLED',
                        'FilterStrength' => 1,
                        'InputFilter' => 'AUTO',
                        'SourceEndBehavior' => 'CONTINUE',
                    ],
                ],

            ],
            'InputSpecification' => [
                'Codec' => 'AVC',
                'MaximumBitrate' => 'MAX_10_MBPS',
                'Resolution' => 'HD',
            ],
            'LogLevel' => 'DISABLED',
            'Name' => 'instagram',
        ])->get('Channel');

    }

    public function start()
    {

        $this->medialive->startChannel([
            'ChannelId' => $this->channel['Id'],
        ]);
        $this->running = true;
    }

    public function isRunning()
    {
        return $this->running;
    }

    public function stop()
    {
        $this->medialive->stopChannel([
            'ChannelId' => $this->channel['Id'],
        ]);
        $this->medialive->deleteChannel([
            'ChannelId' => $this->channel['Id'],
        ]);
        $this->medialive->deleteInput([
            'InputId' => $this->input['Id'],
        ]);
        $this->running = false;
    }
}