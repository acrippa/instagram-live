<?php

namespace BigBadWolf;

use Ratchet\MessageComponentInterface;
use Ratchet\ConnectionInterface;
use React\EventLoop\LoopInterface;

class InstagramServer implements MessageComponentInterface
{

    protected $clients;
    private $instagramLive;
    private $loop;

    public function __construct(LoopInterface $loop)
    {
        $this->loop = $loop;
        $this->clients = new \SplObjectStorage;
        if (getenv('DEBUG')) {
            $logger = new \Monolog\Logger('rtc');
            $logger->pushHandler(new \Monolog\Handler\StreamHandler('php://stdout', \Monolog\Logger::INFO));
        } else {
            $logger = null;
        }
        $this->instagramLive = new InstagramLive($this, $logger);
    }

    public function onOpen(ConnectionInterface $conn)
    {
        $this->clients->attach($conn);
        echo "New connection! ({$conn->resourceId})\n";
        $responses = $this->instagramLive->status();
        foreach ($responses as $response) {
            $conn->send(json_encode($response));
        }
    }

    public function onMessage(ConnectionInterface $from, $msg)
    {
        echo sprintf('Received message "%s" from %d' . "\n", $msg, $from->resourceId);
        $json = json_decode($msg);
        $payload = isset($json->payload) ? (array) $json->payload : [];
        try{
            $response = call_user_func_array([$this->instagramLive, $json->action], $payload);
        }catch(\Exception $ex){
            $response = [];
            $response['status'] = 500;
            $response['error'] = $e->getMessage();
        }
        if ($response){
            $this->sendAll($response);
        }
    }

    public function onClose(ConnectionInterface $conn)
    {
        $this->clients->detach($conn);
        echo "Connection {$conn->resourceId} has disconnected\n";
    }

    public function onError(ConnectionInterface $conn, \Exception $e)
    {
        echo "An error has occurred: {$e->getMessage()}\n";
        $this->clients->detach($conn);
        $conn->close();
    }

    public function sendAll($message)
    {
        echo sprintf('Sending message "%s"' . "\n", json_encode($message));
        foreach ($this->clients as $client) {
            $client->send(json_encode($message));
        }
    }

    public function addStatisticsTimer()
    {
        $this->loop->addPeriodicTimer(getenv('SLEEPTIME'), function($timer){
            try{
                $stats = $this->instagramLive->statistics();
                if($stats!==false){
                    $this->sendAll($stats);
                }else{
                    $this->loop->cancelTimer($timer);
                }
            }catch(\Exception $e){
                $this->sendAll([
                    'status' => 500,
                    'error' => $e->getMessage(),
                ]);
            }
        });
    }

    public function addCommentsTimer()
    {
        $this->loop->addPeriodicTimer(getenv('SLEEPTIME'), function($timer){
            try{
                $comments = $this->instagramLive->comments();
                if($comments!==false){
                    $this->sendAll($comments);
                }else{
                    $this->loop->cancelTimer($timer);
                }
            }catch(\Exception $ex){
                $this->sendAll([
                    'status' => 500,
                    'error' => $e->getMessage(),
                ]);
            }
        });
    }
}