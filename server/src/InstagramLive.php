<?php

namespace BigBadWolf;

use InstagramAPI\Instagram;
use InstagramAPI\Exception\ChallengeRequiredException;
use BigBadWolf\Api\Api;
use BigBadWolf\Broadcaster\Factory as BroadcasterFactory;
use BigBadWolf\Statistics\Factory as StatisticsFactory;

class InstagramLive
{
    private $broadcastId;
    private $broadcastProcess;
    private $broadcaster;
    private $comments;
    private $connections;
    private $instagram;
    private $loginResponse;
    private $logger;
    private $password;
    private $server;
    private $streamUploadUrl;
    private $username;

    public function __construct($server, $logger)
    {
        $this->server = $server;
        $this->logger = $logger;
        $this->connections = [];
        $this->instagram = new Instagram(getenv('DEBUG'), getenv('TRUNCATED_DEBUG'));
    }

    public function status()
    {
        $responses = [];
        $responses[] = [
            'status' => 200,
            'namespace' => 'user',
            'mutation' => 'status',
            'username' => $this->instagram->username,
            'password' => $this->instagram->password,
            'loggedIn' => $this->instagram->isMaybeLoggedIn,
        ];
        if($this->broadcastId){
            $responses[] = [
                'status' => 200,
                'namespace' => 'broadcast',
                'mutation' => 'broadcasting',
                'broadcastId' => $this->broadcastId,
                'broadcasting' => $this->broadcaster ? $this->broadcaster->isRunning() : false,
                'info' => $this->instagram->live->getInfo($this->broadcastId),
            ];
        }
        return $responses;
    }

    /*
     * Check which broadcaster is available on the machine
     * To use OBS or ffmpeg the programs must be installed
     */
    public function capabilities()
    {

    }

    public function login($username, $password)
    {
        try{
            $this->username = $username;
            $this->password = $password;
            $this->loginResponse = $this->instagram->login($username, $password);
            if ($this->loginResponse !== null && $this->loginResponse->isTwoFactorRequired()) {
                $response['namespace']='user';
                $response['status'] = 200;
                $response['action'] = 'twofactor';
            }else{
                $this->user = $this->instagram->account->getCurrentUser()->getUser();
                $response['namespace']='user';
                $response['status'] = 200;
                $response['accountId'] = $this->instagram->account_id;
                $response['mutation'] = 'login';
            }
            $response['namespace']='user';
        }catch(ChallengeRequiredException $e){
            if($e->getResponse()->getErrorType() === 'checkpoint_challenge_required'){
                $this->loginResponse = $e->getResponse();
                $response['namespace']='user';
                $response['status'] = 200;
                $response['action'] = 'challenge';
            }else{
                $response['status'] = 500;
                $response['error'] = $e->getMessage();
            }
        }catch (\Exception $e) {
            $response['status'] = 500;
            $response['error'] = $e->getMessage();
        }
        return $response;
    }

    public function twoFactor($code)
    {
        try{
            $twoFactorIdentifier = $this->loginResponse->getTwoFactorInfo()->getTwoFactorIdentifier();
            $verificationCode = trim($code);
            $this->instagram->finishTwoFactorLogin($this->instagram->username, $this->instagram->password, $twoFactorIdentifier, $verificationCode);
            $this->user = $this->instagram->account->getCurrentUser()->getUser();
            $response['status'] = 200;
            $response['accountID'] = $this->instagram->account_id;
            $response['namespace']='user';
            $response['mutation'] = 'login';
        }catch (\Exception $e) {
            $response['status'] = 500;
            $response['error'] = $e->getMessage();
        }
        return $response;
    }

    public function challenge($method)
    {
        $method = $method === 'sms' ? 0 : 1;
        $checkApiPath = trim(substr($this->loginResponse->getChallenge()->getApiPath(), 1));
        $challengeResponse = $this->instagram->request($checkApiPath)
        ->setNeedsAuth(false)
        ->addPost('choice', $method)
        ->addPost('_uuid', $this->instagram->uuid)
        ->addPost('guid', $this->instagram->uuid)
        ->addPost('device_id', $this->instagram->device_id)
        ->addPost('_uid', $this->instagram->account_id)
        ->addPost('_csrftoken', $this->instagram->client->getToken())
        ->getDecodedResponse();
        $response['status'] = 200;
        $response['namespace']='user';
        if ($challengeResponse['status'] === 'ok' && isset($challengeResponse['action']) && $challengeResponse['action'] === 'close'){
            $response['action'] = 'close';
        }else{
            $response['action'] = 'challenged';
        }
        return $response;
    }

    public function challengeCode($code)
    {
        $checkApiPath = trim(substr($this->loginResponse->getChallenge()->getApiPath(), 1));
        $this->instagram->changeUser($this->username, $this->password);
        $challengeResponse = $this->instagram->request($checkApiPath)
        ->setNeedsAuth(false)
        ->addPost('security_code', $code)
        ->addPost('_uuid', $this->instagram->uuid)
        ->addPost('guid', $this->instagram->uuid)
        ->addPost('device_id', $this->instagram->device_id)
        ->addPost('_uid', $this->instagram->account_id)
        ->addPost('_csrftoken', $this->instagram->client->getToken())
        ->getDecodedResponse();
        $response['status'] = 200;
        $response['namespace']='user';
        if (@$challengeResponse['status'] === 'ok' && @$challengeResponse['logged_in_user']['pk'] !== null) {
            $response['action'] = 'close';
        } else {
            $response['action'] = 'retry';
        }
        return $response;
    }

    public function logout()
    {
        $this->instagram->logout();
        return [
            'status' => 200,
            'namespace' => 'user',
            'mutation' => 'logout'
        ];
    }

    public function createBroadcast($source, $destination)
    {
        $this->stream = $this->instagram->live->create();
        $this->broadcastId = $this->stream->getBroadcastId();
        $this->comments = true;
        $this->streamUploadUrl = preg_replace(
            '#^rtmps://([^/]+?):443/#ui',
            'rtmp://\1:80/',
            $this->stream->getUploadUrl()
        );
        $split = preg_split("[" . $this->broadcastId . "]", $this->streamUploadUrl);
        $streamUrl = trim($split[0]);
        $streamKey = trim($this->broadcastId . $split[1]);
        $this->broadcaster = BroadcasterFactory::create($destination->type);
        $this->broadcaster->addSource($source);
        $this->broadcaster->addDestination($this->streamUploadUrl);
        Api::send(
            '',null, 
            [
                'id' => $this->broadcastId,
                'user_id' => $this->user->getPk(),
            ]
        );
        return [
            'status' => 200,
            'namespace' => 'broadcast',
            'mutation' => 'creating',
            'broadcastId' => $this->broadcastId,
            'streamUploadUrl' => $this->streamUploadUrl,
            'streamUrl' => $streamUrl,
            'streamKey' => $streamKey,
            'comments' => $this->comments,
        ];
    }

    public function toggleComments()
    {
        $this->comments = !$this->comments;
        if($this->comments){
            $this->instagram->live->enableComments($this->broadcastId);
        }else{
            $this->instagram->live->disableComments($this->broadcastId);
        }
        return [
            'status' => 200,
            'namespace' => 'comment',
            'mutation' => 'toggle',
            'comments' => $this->comments,
        ];
    }

    public function startBroadcast()
    {
        $this->statistics = StatisticsFactory::create(getenv('STATS_PERSISTENCE'), $this->instagram, $this->broadcastId);
        $this->broadcaster->start();
        $this->instagram->live->start($this->broadcastId);
        $this->lastComment = 0;
        $this->server->addStatisticsTimer();
        $this->server->addCommentsTimer();
        return [
            'status' => 200,
            'namespace' => 'broadcast',
            'mutation' => 'broadcasting',
            'broadcasting' => true,
            'info' => $this->instagram->live->getInfo($this->broadcastId),
        ];
    }

    public function statistics()
    {
        if($this->broadcaster->isRunning()){
            $stats = $this->statistics->get();
            Api::send(
                'statistics',
                $this->broadcastId,
                $stats
            );
            return array_merge([
                'status' => 200,
                'namespace' => 'statistics',
                'action' => 'status',
            ], $stats);
        }
        return false;
    }

    public function comments()
    {
        if($this->broadcaster->isRunning()){
            $response = $this->instagram->live->getComments(
                $this->broadcastId,
                $this->lastComment ? $this->lastComment : null,
                getenv('COMMENTS')
            );
            $comments = [];
            $systemComments = $response->getSystemComments();
            if(!empty($systemComments)){
                $this->lastComment = $systemComments[0]->getCreatedAt();
            }
            foreach($response->getComments() as $comment){
                $comments[] = [
                    'id' => $comment->getPk(),
                    'username' => $comment->getUser()->getUsername(),
                    'avatar' => $comment->getUser()->getProfilePicUrl(),
                    'text' => $comment->getText(),
                    'timestamp' => $comment->getCreatedAt(),
                ];
            }
            if(!empty($comments)){
                $this->lastComment = max($this->lastComment, $comments[0]['timestamp']);
            }
            Api::send(
                'comments', 
                $this->broadcastId,
                ['comments' => $comments]
            );
            return [
                'status' => 200,
                'namespace' => 'comment',
                'comments' => $comments,
                'mutation' => 'comments',
            ];
        }
        return false;
    }

    public function sendComment($comment)
    {
        $response = $this->instagram->live->comment($this->broadcastId, $comment);
        $timestamp = $response->getComment()->getCreatedAt();
        $this->lastComment = max($this->lastComment, $timestamp);
        return [
            'status' => 200,
            'namespace' => 'comment',
            'comment' => [
                'username' => $this->instagram->username,
                'avatar' => $this->user->getProfilePicUrl(),
                'text' => $comment,
                'timestamp' => $timestamp,
            ],
            'mutation' => 'comment',
        ];
    }

    public function stopBroadcast($save = false)
    {
        $this->instagram->live->end($this->broadcastId);
        $this->broadcaster->stop();
        $this->broadcasting = false;
        Api::send(
            'stop', 
            $this->broadcastId,
            []
        );
        if ($save){
            $this->instagram->live->addToPostLive($this->broadcastId);
        }
        return [
            'status' => 200,
            'namespace' => 'broadcast',
            'mutation' => 'stopped',
        ];
    }

}
