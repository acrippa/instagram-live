<?php

namespace BigBadWolf\Api;

class Api
{
    public static function send($command, $broadcastId, $data)
    {
        $apiUrl = getenv('API_URL');
        $apiToken = getenv('API_TOKEN');
        if($apiUrl && $apiToken){
            $url = $apiUrl . '/' . $broadcastId . '/' . $command;
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, $url);
            curl_setopt($ch, CURLOPT_POST, 1);
            curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($data));
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_HTTPHEADER, [
                'Authorization: Bearer ' . $apiToken
            ]);
            $server_output = curl_exec ($ch);
            curl_close ($ch);
        }
    }
}
