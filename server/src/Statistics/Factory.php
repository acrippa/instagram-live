<?php

namespace BigBadWolf\Statistics;

class Factory
{
    public static function create(String $type, $instagram, $broadcastId)
    {
        switch($type){
            case 'csv':
                return new Csv($instagram, $broadcastId);
            break;
            default:
                return new Volatile($instagram, $broadcastId);
            break;
        }
    }
}