<?php

namespace BigBadWolf\Statistics;

class Csv extends Statistics
{

    private function save($stats)
    {
        $time = time();
        $fh = fopen('./'.$this->broadcastId.'csv', 'a');
        $data = [
            [$time, 'likes', $stats['likes']],
            [$time, 'burst_likes', $stats['bursts']],
            [$time, 'viewer_count', $stats['viewers']],
            [$time, 'total_viewers', $stats['total_viewers']]
        ];
        fputcsv($fh, $data);
    }

    public function get()
    {
        $stats = parent::get();
        $this->save($stats);
        return $stats;
    }
}