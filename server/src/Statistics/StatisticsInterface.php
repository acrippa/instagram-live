<?php

namespace BigBadWolf\Statistics;

interface StatisticsInterface
{
    public function get();
}