<?php

namespace BigBadWolf\Statistics;

class Statistics implements StatisticsInterface
{

    private $broadcastId;
    private $instagram;

    public function __construct($instagram, $broadcastId)
    {
        $this->instagram = $instagram;
        $this->broadcastId = $broadcastId;
    }

    public function get()
    {
        $likes = $this->instagram->live->getLikeCount($this->broadcastId);
        $heartbeat = $this->instagram->live->getHeartbeatAndViewerCount($this->broadcastId);
        return [
            'likes' => $likes->getLikes(),
            'bursts' => $likes->getBurstLikes(),
            'viewers' => $heartbeat->getViewerCount(),
            'total_viewers' => $heartbeat->getTotalUniqueViewerCount()
        ];
    }
}