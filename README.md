# InstagramLive
A proof of concept, just to play around with [Ratchet](http://www.socketo.me/) and [VueJs](https://www.vuejs.org)

All this couldn't be made without the amazing [mgp25's Instagram Private API](https://github.com/mgp25/Instagram-API/).

# Setup
After running [Composer](https://getcomposer.org/download/) you can personalize the installation
copying the file `server/.env.example` to `server/.env` and modifying it to match your preferences/setup.

To run the server change your current folder to server and run the `index.php` file (`php index.php`).By default it creates a websocket server on localhost:8080

If you don't want to use a webserver to serve the client interface you can use the php built-in server:
change the folder to `client\public`, run `php -S localhost:$port` and point your browser to that address
You can also directly open the `index.html` file in the public folder.

In the backend folder there is a simple api made with [Lumen](https://lumen.laravel.com/) where you can send the collected data (statistics and comments).
To use this feature use the same env variable `API_TOKEN` in `backend\.env` and `server\.env` and modify the `API_URL` to match your api server address

It's possible to build a complete app to help analyse this data, but it's beyond the scope of this little experiment

# Note
As a PoC it doesn't have an error proof exception handling and the annoying instagram challenge, I never had the chance to test it, I'll probably fix it in the future. 
The best way to use it is to create a stream key using the direct broadcaster and use your favorite encoding software to push the stream to the instagram cdn.